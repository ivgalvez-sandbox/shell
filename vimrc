" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

" Configuration file for vim
"set runtimepath=~/.vim,/etc/vim,/usr/share/vim/vimfiles,/usr/share/vim/addons,/usr/share/vim/vim70,/usr/share/vim/vimfiles,/usr/share/vim/addons/after,~/.vim/after

" Normally we use vim-extensions. If you want true vi-compatibility
" remove change the following statements
set nocompatible  " Use Vim defaults instead of 100% vi compatibility
set backspace=2   " more powerful backspacing
" set mouse=a       " Activate mouse

" Now we set some defaults for the editor
set autoindent !  " always set autoindenting off
set paste 
"set linebreak    " Don't wrap words by default
set textwidth=0      " Don't wrap lines by default
set viminfo='20,\"100 " read/write a .viminfo file, don't store more than
         " 100 lines of registers
set history=100    " keep 100 lines of command line history
set ruler      " show the cursor position all the time
set tabstop=3
set shiftwidth=3

" Use F5 to toggle spell checking
map <F5> :setlocal spell! spelllang=en_us<CR> 
" Use F2 to paste chunks of code correctly
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

" Specify a backup directory
set backup     " Don't keep a backup file
set backupdir=/tmp/$USER

" Suffixes that get lower priority when doing tab completion for filenames.
" These are files we are not likely to want to edit or read.
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc

" We know xterm-debian is a color terminal
if &term =~ "xterm-debian" || &term =~ "xterm-xfree86"
  set t_Co=16
  set t_Sf=^[[3%dm
  set t_Sb=^[[4%dm
endif

" Make p in Visual mode replace the selected text with the "" register.
vnoremap p <Esc>:let current_reg = @"<CR>gvdi<C-R>=current_reg<CR><Esc>

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if &t_Co > 1
	syntax enable
endif
"syntax on

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
" set background=dark

if has("autocmd")
 " Enabled file type detection
 " Use the default filetype settings. If you also want to load indent files
 " to automatically do language-dependent indenting add 'indent' as well.
 filetype plugin on
endif " has ("autocmd")

" Some Debian-specific things
augroup filetype
  au BufRead reportbug.*      set ft=mail
  au BufRead reportbug-*      set ft=mail
augroup END

" Set paper size from /etc/papersize if available (Debian-specific)
if filereadable('/etc/papersize')
  let s:papersize = matchstr(system('/bin/cat /etc/papersize'), '\p*')
  if strlen(s:papersize)
    let &printoptions = "paper:" . s:papersize
  endif
  unlet! s:papersize
endif

" The following are commented out as they cause vim to behave a lot
" different from regular vi. They are highly recommended though.
"set showcmd      " Show (partial) command in status line.
"set showmatch    " Show matching brackets.
"set ignorecase      " Do case insensitive matching
"set incsearch    " Incremental search
"set autowrite    " Automatically save before commands like :next and :make


highlight Cursor ctermbg=Green ctermfg=NONE gui=NONE
highlight Normal ctermbg=black ctermfg=grey gui=NONE
highlight Constant ctermbg=black ctermfg=yellow gui=NONE
highlight Special ctermbg=black ctermfg=red gui=NONE
highlight Comment ctermbg=black ctermfg=green gui=NONE
highlight Statement ctermbg=black ctermfg=cyan gui=NONE
highlight PreProc ctermbg=black ctermfg=white gui=NONE
highlight Type ctermbg=black ctermfg=lightblue gui=NONE
highlight Identifier ctermbg=black ctermfg=magenta gui=NONE
highlight NonText ctermbg=grey

augroup filetypedetect
au BufNewFile,BufRead *.conf      setf cfg
au BufNewFile,BufRead *.conf.skel setf cfg
au BufNewFile,BufRead *.default   setf cfg
au BufNewFile,BufRead *.pc        setf cpp
augroup END

"Highlighting of Doxygen tags
" let g:load_doxygen_syntax=1
  
