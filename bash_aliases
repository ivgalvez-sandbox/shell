#############
### Alias ###
#############

# Enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then
	eval `dircolors -b`
	alias ls='ls --color=auto'
	alias la='ls -la --color=auto'
	alias ll='ls -l --color=auto'
	alias l='ls -lF --color=auto'
	alias lh='ls -l --color=auto *.h'
	alias lc='ls -l --color=auto *.c *.cc *.cpp'
	alias dir='ls --color=auto --format=vertical'
	alias vdir='ls --color=auto --format=long'
fi

# Vim as a pager - similar to less command but with color
alias vless='/usr/share/vim/vimcurrent/macros/less.sh'

# Some file management aliases
alias m='more'
alias cd..='cd ..'
alias rm='rm -i '
alias du='du -h '
alias deltree='rm -rf '
alias df='df -h'

# Search and substitute utilities
alias grep='grep --color=tty -d skip'
alias fgrep='fgrep --color=tty -d skip'
alias egrep='egrep --color=tty -d skip'
alias findc='find . -follow -name "*.c*" 2>/dev/null | grep -v ".svn" | grep -v ".cmake" | xargs grep --color=tty -I -d skip -n'
alias findh='find . -follow -name "*.h*" 2>/dev/null | grep -v ".svn" | grep -v ".html" | xargs grep --color=tty -I -d skip -n'
alias findf='find . -follow -name' 
alias replace='replace.sh'

#  Process management
alias proc='ps -ef |grep $LOGNAME'
alias pro='ps -ef |grep '

# Build utilities
alias borra='findf "*.o" | xargs rm; findf core | xargs rm'
alias clean='rm -f *.changes *.gz *.dsc'
alias make='nice make -j3'
alias makeout='make > out.txt 2>&1'
alias makeerror='grep -v "ndds.4.1e" out.txt |grep -v "ACE" |grep -v "KerTrace.h" |grep -v "kdata" |grep -v "kerdata"'
alias indexa='ctags -R -h ".cc.cpp.h"'
alias limpia='limpia.sh 2>/dev/null'
alias build=build.sh

# Subversion and git tricks
#alias propset='svn propset svn:keywords "Author Id HeadURL Revision Date" '
alias gitlog='git log --graph --full-history --all --color --pretty=format:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s"'

# Network aliases
alias sshclean='rm $HOME/.ssh/known_hosts'

# Autotools
#alias configure='aclocal && autoheader && libtoolize --copy --force && automake --add-missing --copy &&autoconf && ./configure && make'
#alias autotools='aclocal && autoheader && libtoolize --copy --force && automake --add-missing --copy &&autoconf && ./configure'

# Debian packages
#alias package='fakeroot make debpkg'

# Terminal trash
#alias trash_on="export TRASH_OFF=NO"
#alias trash_off="export TRASH_OFF=YES"
